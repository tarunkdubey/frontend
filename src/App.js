import logo from './logo.svg';
import './App.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Service from "./services/service";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure()
class App extends React.Component {

    state = {
        team: '',
        insertTeamName:'',
        insertRoleName:'',
        teamRolesList:[],
        teamRolesListCalled :false
    };
    constructor() {
        super();
        this.onSubmitTeam = this.onSubmitTeam.bind(this)
        this.onSubmitTeamRole = this.onSubmitTeamRole.bind(this)
    }
    onChangeTeamName = (event) => {
        this.setState({team: event.target.value});
    };
    onChangeInsertTeamName = (event) => {
        this.setState({insertTeamName: event.target.value});
    };
    onChangeInsertRoleName = (event) => {
        this.setState({insertRoleName: event.target.value});
    };

    async onSubmitTeam() {
        let list = await Service.getTeamRole(this.state.team)
        this.setState({
           teamRolesList:list.data,
            teamRolesListCalled:true
        });
    };

    async onSubmitTeamRole() {
        let body = JSON.stringify({ "team_name": this.state.insertTeamName,
        "role_name": this.state.insertRoleName})
        let res = await Service.createTeamRole(body)
        if(res != null) {
            toast("Team role created successfully")
        }
    };

    renderTableData() {
        return this.state.teamRolesList.map((teamRole, index) => {
            const { team_name, role_name } = teamRole //destructuring
            return (
                <tr key={team_name}>
                    <td>{team_name}</td>
                    <td>{role_name}</td>
                </tr>
            )
        })
    }
    renderTableHeader() {
        let header = Object.keys(this.state.teamRolesList[0])
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    render() {
            let length = this.state.teamRolesList.length
            return (
                <div className="flex-container">
                <div className="flex-child">
                        <div>
                            <div className="form-group">
                                <label htmlFor="title">Team Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="title"
                                    required
                                    value={this.state.team}
                                    onChange={(e) => {this.onChangeTeamName(e)}}
                                    name="title"
                                />
                            </div>

                            <button onClick={this.onSubmitTeam} className="btn btn-success">
                                Submit
                            </button>
                        </div>
                    {this.state.teamRolesList.length?
                    <div>
                        <table id='teamRoles'>
                            <tbody>
                            <tr>{this.renderTableHeader()}</tr>
                            {this.renderTableData()}
                            </tbody>
                        </table>
                    </div>:null}
                </div>
                    <div className="flex-child">
                        <div>
                            <div className="form-group">
                                <label htmlFor="title">Team Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="title"
                                    required
                                    value={this.state.insertTeamName}
                                    onChange={(e) => {this.onChangeInsertTeamName(e)}}
                                    name="title"
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="title">Role Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="title"
                                    required
                                    value={this.state.insertRoleName}
                                    onChange={(e) => {this.onChangeInsertRoleName(e)}}
                                    name="title"
                                />
                            </div>

                            <button onClick={this.onSubmitTeamRole} className="btn btn-success">
                                Submit
                            </button>
                        </div>

                    </div>
                </div>
            );
        }
    }

export default App;
