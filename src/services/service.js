import axios from 'axios';
import {toast} from "react-toastify";
const EMPLOYEE_API_BASE_URL = "http://127.0.0.1:5000/api";

class Service {

    async createTeamRole(teamRole){

        return await axios.post(EMPLOYEE_API_BASE_URL +'/createTeamRole', teamRole, {
             headers: {
                 'Content-Type': 'application/json',
             }}).catch((reason) => {
            toast(reason.response.data.error)
        })
    }

     async getTeamRole(teamName){

         return await axios.get(EMPLOYEE_API_BASE_URL + '/getTeamRole?team_name='+teamName)
    }
}

export default new Service()
